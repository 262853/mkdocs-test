#!/bin/bash

for page in $(find "$(pwd)" -type f | grep -i '.md')
do

sed -Ei "/Poznámka/,/^\s*$/ s/^/\t/g;s/\t\*\*Poznámka[.:]\*\* ?(.*)/\!\!\! note \"Poznámka\"\n\t\1/" $page

sed -Ei "/Pozor/,/^\s*$/ s/^/\t/g;s/\t\*\*Pozor!\*\* ?(.*)/\!\!\! warning \"Pozor\"\n\t\1/" $page

echo "Alerty doplneny do $page."

done