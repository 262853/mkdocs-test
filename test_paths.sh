#!/bin/bash

for page in $(find "./docs" -type f | grep -i '.md')
do
    
    sed -Ei 's|\]\(/|](/moodledocs/|g' $page

done
